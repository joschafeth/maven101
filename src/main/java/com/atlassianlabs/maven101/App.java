package com.atlassianlabs.maven101;

/**
 * Hello world!
 */
public class App {
    public static void main(String[] args) {
        if (args.length != 1) {
            throw new RuntimeException("argument missing!");
        }
        System.out.println("Hello " + args[0] + "!");
    }
}
